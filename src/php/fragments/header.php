<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="">
      <meta name="description" content="">

      <title>Find Tenants Quickly! - Everent</title>

      <link href="css/bootstrap.css" rel="stylesheet">
      <link href="css/style.css" rel="stylesheet">
      <link href="css/animate.css" rel="stylesheet">
      <link href="css/style-magnific-popup.css" rel="stylesheet">

      <link href="css/main.css" rel="stylesheet">
      <link href="css/fonts.css" rel="stylesheet">

      <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,100' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700' rel='stylesheet' type='text/css'>

      <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
      <!--[if lt IE 9]>
        <script src="http://demo.epic-webdesign.com/tf-spectrum/v1/js/html5shiv.js"></script>
        <script src="jhttp://demo.epic-webdesign.com/tf-spectrum/v1/s/respond.min.js"></script>
      <![endif]-->

      <link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">

      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-130451527-1', 'auto');
        ga('send', 'pageview');
      </script>

      <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
      <script src="https://simpliprotected.com/javascript/twitter.js"></script>

      <script>
          $(document).ready(function() {
              $.getJSON('api/twitter.php?url='+encodeURIComponent('statuses/user_timeline.json?screen_name=everentco&count=3'), function(tweets) {
                  $("#twitter").html(tz_format_twitter(tweets));
              })
          })
      </script>
  </head>
  <body>
      <header class="header">
          <nav class="navbar navbar-default navbar-fixed-top">
              <div class="container">
                  <div class="navbar-header">
                      <a href="/" class="navbar-brand brand scrool"><img src="images/logo.svg"></a>
                  </div>
                  <div id="navbar-collapse-02" class="collapse navbar-collapse">
                      <ul class="nav navbar-nav navbar-right">
                          <li><a href="<?php echo $page == 'home' ? '' : '/' ?>#benefits" class="<?php echo $page != 'home' ? 'external' : 'scrool' ?>">Benefits</a></li>
                          <li><a href="<?php echo $page == 'home' ? '' : '/' ?>#features" class="<?php echo $page != 'home' ? 'external' : 'scrool' ?>">Features</a></li>
                          <li><a href="<?php echo $page == 'home' ? '' : '/' ?>#pricing" class="<?php echo $page != 'home' ? 'external' : 'scrool' ?>">Pricing</a></li>
                          <li><a href="<?php echo $page == 'home' ? '' : '/' ?>#contact" class="<?php echo $page != 'home' ? 'external' : 'scrool' ?>">Contact</a></li>
                          <li><a href="blog.everent.co" class="external">Blog</a></li>
					  </ul>
                  </div>
              </div>
          </nav>
      </header>

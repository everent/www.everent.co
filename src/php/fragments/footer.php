    <section class="section-dark" id="useful">
        <div class="container">
            <div class="row">
                <div class="col-md-4 about">
                    <h5>About Everent</h5>
                    <p>We believe getting your vacancy filled should be <b>simple</b> and <b>quick</b>.&nbsp;&nbsp;Everything was designed so that you can easily bring in new tenants without hassle.
                     <p>We created Everent so that you can <b>rent to quality tenants quickly!</b></p>
                </div>
                <div class="col-md-3 links">
                    <h5>Important Links</h5>
                    <ul>
                        <li><a href="http://blog.everent.co">Blog</a></li>
                        <li><a href="/terms">Terms of Service</a></li>
                        <li><a href="/privacy">Privacy Policy</a></li>
                        <li><a href="mailto:contact@everent.co">Contact</a></li>
                    </ul>
                </div>
                <div class="col-md-5 tweets">
                    <h5>Recent Tweets</h5>
                    <ul id="twitter"></ul>
                </div>
            </div>
        </div>
    </div>
    </section>
    <div class="footer">
        <div class="container">
            <div class="col-xs-10 col-md-10">
                <div class="copyright">
                    <p>Copyright&nbsp;©<?php echo date("Y"); ?>&nbsp;<b>Everent,&nbsp;LLC</b>.&nbsp;&nbsp;All&nbsp;Rights&nbsp;Reserved.</p>
                </div>
            </div>
            <div class="col-xs-2 col-md-2 text-right">
                <ul class="footer_social pull-right">
                    <li><a href="https://www.facebook.com/everentco"><i class="icon icon-facebook"></i></a></li>
                    <li><a href="https://twitter.com/everentco"><i class="icon icon-twitter"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</body>

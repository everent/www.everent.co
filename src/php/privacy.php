<?php include_once("fragments/header.php"); ?>


    <style>
        h4 {
            margin-bottom: 5px;
            margin-top: 30px;
        }
    </style>

    <section class="section-white">
        <div class="container">
            <div class="row">
                <div class="col-lg-12" style="margin-left:15px">
                    <h3>Privacy Policy</h3>
                    <h5>Last Updated: December 4, 2018</h5>
                    <p>
                        Everent LLC (“Everent”, “us”, “we”, or “our”) operates the <a href="http://everent.co">Everent.co</a> website (the “Service”). The following is our Privacy Policy (“Policy”) that states our policies regarding the collection, use and disclosure of personal information that you provide our Service. “Personal Information” that we may collect and use includes but is not limited to: email address; phone number; name; mailing address; billing address and other information. This information is collected to improve the Service; communicate with you regarding your purchase and service; billing; etc. Any use or dissemination by us of your personal information is bound by the policy described below. We will not share your personal information with any third party not listed here.  By using the Service, you agree to the collection and use of information in accordance with this policy. The terms and conditions listed in this Policy have the same meaning as the terms and conditions in our Terms of Service unless otherwise specified or defined.
                    </p>
                    <h4>1. Log Data</h4>
                    <p>
                        We collect information that your browser sends whenever you visit our Service (“Log Data”). This Log Data may include information such as your computer's Internet Protocol (“IP”) address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics. In addition, we may use third party services such as Google Analytics that collect, monitor and analyze this type of information in order to increase our Service's functionality. These third party service providers have their own privacy policies addressing how they use such information.
                    </p>
                    <h4>2. Cookies</h4>
                    <p>
                        Cookies are files with a small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and transferred to your device. We use cookies to collect information in order to improve our services for you. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. The Help feature on most browsers provide information on how to accept cookies, disable cookies or to notify you when receiving a new cookie. If you do not accept cookies, you may not be able to use some features of our Service and we recommend that you leave them turned on.
                    </p>
                    <h4>3. Service Providers</h4>
                    <p>
                        We may employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services and/or to assist us in analyzing how our Service is used. These third parties have access to your Personal Information only to perform specific tasks on our behalf and are obligated not to disclose or use your information for any other purpose.
                    </p>
                    <h4>4. Communications</h4>
                    <p>
                        We may use your Personal Information to contact you with newsletters, marketing or promotional materials and other information that may be of interest to you. You may opt out of receiving any, or all, of these communications from us by following the unsubscribe link or instructions provided in any email we send.
                    <h4>5. Compliance with Laws</h4>
                    <p>
                        Disclosure of your Personal Information may occur when required by law or subpoena. Disclosure may also occur if we ascertain that disclosure is necessary to comply with reasonable requests from law enforcement or to protect the security of our Service.
                    </p>
                    <h4>6. Business Transaction</h4>
                    <p>
                        If Everent is involved in a merger, acquisition or sale, all personal information may be transferred as a business asset. Notice will be provided if your Personal information is transferred and/or falls under a new or updated Privacy Policy.
                    </p>
                    <h4>7. Security</h4>
                    <p>
                        Everent has in place specific security measures in place to protect your personal information from unauthorized third parties. However, any information transferred over the internet or stored electronically is subject to interception, loss or modification. Everent cannot guarantee complete security of your Personal Information.
                    </p>
                    <h4>8. Data Transfer</h4>
                    <p>
                        Your Personal Information is stored in computers that may be located out of your state, province, or country where personal information protection laws differ. Your Personal Information is governed by the jurisdiction in which it is stored. By consenting to this policy and submitting your Personal Information, you accept that your data may be transferred out of your home jurisdiction.
                    </p>
                    <h4>9. Children's Privacy</h4>
                    <p>
                        Only persons age 18 or older have permission to access our Service. Our Service does not address anyone under the age of 13 (“Children”). We do not knowingly collect personally identifiable information from children under 13. If you are a parent or guardian and you learn that your Children have provided us with Personal Information, please contact us. If we become aware that we have collected Personal Information from a child under age 13 without verification of parental consent, we take steps to remove that information from our servers.
                    </p>
                    <h4>10. Changes to This Privacy Policy</h4>
                    <p>
                        This Privacy Policy is effective as of December 4, 2018 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page. We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the Service after we post any modifications to the Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified Privacy Policy. If we make any material changes to this Privacy Policy, we will notify you either through the email address you have provided us, or by placing a prominent notice on our website.
                    </p>
                </div>
            </div>
        </div>
    </section>

<?php include_once("fragments/footer.php"); ?>

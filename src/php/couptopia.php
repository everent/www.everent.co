<?php include_once("fragments/header.php"); ?>

	<style>
		iframe {
			height: 1500px;
			width: 100%;
		}
	</style>
	<section class="section-white" style="padding: 76px 0px !important">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<img src="https://simpliprotected.com/images/couptopia.png" style="max-width: 350px; width: 100%">
					<div class="separator_wrapper">
						<i class="icon icon-star-two red"></i>
					</div>
				</div>
			</div>
			<div class="row" id="welcome">
				<div class="col-md-12 text-center">
					<h2 style="margin-bottom: 15px">We thank you for your purchase!</h2>
					<div style="margin-bottom: 5px">Only a few steps remain.&nbsp;&nbsp;Fill out the form below and we will be in touch very soon!</div>
				</div>
			</div>
			<div class="row margin-top-20" id="info">
				<div class="col-lg-12">
					<iframe src="https://goo.gl/forms/bRzEdG3M8fCKuVJm2"></iframe>
				</div>
			</div>
		</div>
	</section>

<?php include_once("fragments/footer.php"); ?>

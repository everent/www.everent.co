<?php include_once("fragments/header.php"); ?>

    <div class="slider">
        <div class="slide slide-1">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-12">
						<h1 class="title">Quality Tenants Quickly!</h1>
						<p>
							Everent cuts out the pain of marketing, responding, and scheduling showings of your vacant unit.
						</p>
						<p class="subtitle">Find your next tenant today!</p>
						<div class="buttons">
							<a href="/buy" class="btn btn-blue">Sign Up</a>
							<a href="#overview" class="btn btn-white scroll">Learn More</a>
						</div>
					</div>
					<div class="col-lg-6 visible-lg">
						<div class="embed-responsive embed-responsive-16by9 hidden-md" style="overflow: visible">
							<img src="images/rental.svg" style="margin-top: -40px; width: 390px; margin-left: 50px;" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <section class="section-white" id="benefits">
		<div class="container">
            <div class="row margin-bottom-50">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">Benefits</h2>
                    <div class="separator_wrapper">
                        <i class="icon icon-star-two red"></i>
                    </div>
                </div>
            </div>
			<div class="bullets row">
				<div class="col-md-4 service-item">
					<div class="service-inner blue" style="padding-bottom: 10px">
						<i class="fa fa-check" style="color: #2ecc71; font-size: 70px"></i>
						<h3 style="margin-top: 10px">Save Time and Hassle</h3>
						<p>Enter your listing information once and we take care of the rest. No more back and forth communication with phone calls, emails, or texts.  Fit showings to your schedule and reduce no shows.</p>
					</div>
				</div>
				<div class="col-md-4 service-item">
					  <div class="service-inner blue" style="padding-bottom: 10px">
						<i class="fa fa-ban" style="color: red; font-size: 70px"></i>
						<h3 style="margin-top: 10px">Rapidly Schedule High Quality Applicants</h3>
						<p>Applicants are responded to right away, screened based on your criteria, and scheduled immediately when we have their attention span.</p>
					</div>
				</div>
				<div class="col-md-4 service-item">
					<div class="service-inner blue"  style="padding-bottom: 10px">
						<i class="fa fa-lock" style="color: black; font-size: 70px"></i>
						<h3 style="margin-top: 10px">Affordable</h3>
						<p>Built for individual property owners. No subscription costs or ongoing fees. Only use our service when you have a vacancy.</p>
					</div>
				</div>
				<!--
				<div class="col-md-4 service-item">
					<div class="service-inner blue" style="padding-bottom: 10px">
						<i class="fa fa-amazon" style="color: black; font-size: 70px"></i>
						<h3 style="margin-top: 10px">Available on Amazon</h3>
						<p><a href="http://a.co/2HnE5dm">Buy with confidence</a> from your favorite and most trusted online retailer.</p>
					</div>
				</div>
				-->
			</div>
        </div>
    </section>

    <section class="section-grey small-padding-bottom story" id="features">
        <div class="container">
            <div class="row margin-bottom-50">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">Features</h2>
                    <div class="separator_wrapper">
                        <i class="icon icon-star-two red"></i>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="story-block story-right">
                        <div class="story-image"><img src="images/blocked.svg" alt=""></div>
                        <h4>Internet Filter</h4>
                        <p>Our website filter combined with Google and YouTube Safe Search, provides a safe Internet environment for your kids.</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="story-block story-right">
                        <div class="story-image"><img src="images/wifi.svg" alt=""></div>
                        <h4>Separate Network for Your Kids</h4>
                        <p>A filtered and safe Internet environment is created specifically for your kids.&nbsp;&nbsp;The parent's current network is not affected by the Internet filter.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-white" id="pricing">
        <div class="container">
            <div class="row margin-bottom-50">
                <div class="col-md-12 text-center">
                    <h2 class="section-title">Simple Pricing</h2>
                    <div class="separator_wrapper">
                        <i class="icon icon-star-two red"></i>
                    </div>
                    <p class="section-subtitle">There are many variations of passages of Lorem Ipsum available, but the majority<br>have suffered alteration, by injected humour, or new randomised words.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="pricing-box-red">
                        <div class="pricing-top">
                            <h3>Kickstart</h3>
                            <p class="price"><span class="currency white">$</span> <span class="number white">199</span> <span class="month white"></span></p>
                        </div>
                        <div class="pricing-bottom">
                            <ul>
                                <li>Tutorial</li>
                                <li>We get it started for you</li>
                                <li>unlimited updates</li>
                                <li>1 user acounts</li>
                                <li>2 databases</li>
                            </ul>
                            <a href="" class="btn btn-md btn-block btn-pricing-red">SUBSCRIBE TODAY</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="newsletter_wrapper">
        <div class="container">
            <div class="newsletter_box">
                  <div class="row">
                        <div class="col-md-3">
                            <img src="images/email.png" alt="picture" class="padding-top-25">
                        </div>
                        <div class="col-md-9">
                            <div class="newsletter_info">
                                <h2>Write something here...<br>
    							Please enter your email to join our newsletter.</h2>
                                <p class="newsletter_success_box" style="display:none;">We received your message and you'll hear from us soon. Thank You!</p>
                                <form id="newsletter-form" class="newsletter_form" action="php/newsletter.php" method="post">
                                    <input id="email_newsletter" name="nf_email" placeholder="Enter Your Email Address" type="email">
                                    <input value="REGISTER!" id="submit-button-newsletter" type="submit">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $('#submit-button-newsletter').click(function(event) {
                if ($('#email_newsletter').val().indexOf('@') != -1) {
                    $('#submit-button-newsletter').val('Registering...')
                    $.get('api/register?email=' + $('#email_newsletter').val(), {}, function() {
                        $('#submit-button-newsletter').val('Registered!')
                        $('.newsletter_success_box').show()
                    })
                }

                return false;
            })
        })
    </script>
<?php include_once("fragments/footer.php"); ?>
